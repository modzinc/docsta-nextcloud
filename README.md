Copy the repo and create a copy of the docker-compose file to make changes to.

```
version: '3'

services:
  db:
    image: mariadb
    command: --transaction-isolation=READ-COMMITTED --binlog-format=ROW
    restart: always
    volumes:
      - "./dockerconfig/mysql:/var/lib/mysql"
    environment:
      - MYSQL_ROOT_PASSWORD=SuperSamplePassword # same password you put in the db.env file
    env_file:
      - db.env

  redis:
    image: redis:alpine
    restart: always

  app:
    #image: nextcloud:apache
    build:
      context: "./dockerfiles/nextcloud/"
      dockerfile: Dockerfile
    restart: always
    ports:
      - 8080:80
    volumes:
      - "./dockerconfig/nextcloud:/var/www/html"
      #- "/mnt/data:/mnt/data"  ### extra mount for "local storage sharing"
    environment:
      - MYSQL_HOST=db
      - REDIS_HOST=redis
    env_file:
      - db.env
    depends_on:
      - db
      - redis

  cron:
    build:
      context: "./dockerfiles/nextcloud/"
      dockerfile: Dockerfile
    restart: always
    volumes:
      - "./dockerconfig/nextcloud:/var/www/html"
      #- "/mnt/data:/mnt/data" ### extra mount for "local storage sharing"
    entrypoint: /cron.sh
    depends_on:
      - db
      - redis
```

---

Copy nginx config file **subdomain.nextcloud.conf.sample** from ~/nginx to your reverse proxy site-confs folder and update it with the changes to fit your domain and remove the .sample from the end. Pay special attention to the lines at the top to edit your config.php file.

```
# make sure that your dns has a cname set for nextcloud
# assuming this container is called "letsencrypt", edit your nextcloud container's config
# located at /config/www/nextcloud/config/config.php and add the following lines before the ");":
#  'trusted_proxies' => ['letsencrypt'],
#  'overwrite.cli.url' => 'https://nextcloud.your-domain.com/',
#  'overwritehost' => 'nextcloud.your-domain.com',
#  'overwriteprotocol' => 'https',
#
#	This file in the docker container is located at ~./config/config.php
#	For the apps to connect the lines above are extremely important to add to the config.php
#	Use an App Password to connect the desktop app to nextcloud found in Settings -> Security at the bottom
#
# Also don't forget to add your domain name to the trusted domains array. It should look somewhat like this:
#  array (
#    0 => '192.168.0.1:444', # This line may look different on your setup, don't modify it.
#    1 => 'nextcloud.your-domain.com',
#  ),

server {
    listen 443 ssl;

    server_name nextcloud.*;

    include /config/nginx/ssl.conf;

    client_max_body_size 0;

    location / {
        include /config/nginx/proxy.conf;
        resolver 127.0.0.11 valid=30s;
        set $upstream_nextcloud nextcloud; #update to ip of nextcloud instance if required
        proxy_max_temp_file_size 2048m;
        proxy_pass https://$upstream_nextcloud:443; #update port to reflect what is used in docker-compose
    }
}
```

---
